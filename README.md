### What it is
This is a very simple [Unity](www.unity.com) package, it takes an array of meshfilters and outputs a STL file, ready for 3D printing. 

This makes it very easy to output STL files from any given Unity Game Object.

### How to install
Just install it like any other git Unity package. 
- Inside Unity, open the `Package Manager` from the menu item Window.
- In the package manager click on the plus icon in the top left corner
- Click on the option `Add package from git URL...`
- paste in the URL from this repo, with the desired version tag e.g. 
`git@bitbucket.org:AndreasBL/stlunitypackage.git#1.0.1`
- Unity will the import the package into your project.

### How to use it
The package includes a very simple demo scene with one script.

The example can be found here: `Packages/dk.happyhobbies.stlconverter/Example` once the package is imported.

For code usage see the script: `GameObjectToSTL.cs`

### Pull Requests
Pull request are more than welcome!