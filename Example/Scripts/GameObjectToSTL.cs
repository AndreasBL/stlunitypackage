﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace dk.happyhobbies.stlconverter.example
{
    public class GameObjectToSTL : MonoBehaviour
    {
        private List<Rotator> rotators;

        [SerializeField]
        private GameObject GroundObject;

        private void Start()
        {
            var root = CreateObject();
            var child = CreateObject();
            child.transform.SetParent(root.transform);
            child.transform.localPosition = Vector3.right * 2f;

            rotators = new List<Rotator> { root, child };
        }

        private Rotator CreateObject()
        {
            var primitive = GameObject.CreatePrimitive(PrimitiveType.Cube);
            primitive.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
            var rotator = primitive.AddComponent<Rotator>();

            return rotator;
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Drop boxes"))
                rotators.ForEach(r => r.Drop());

            if (GUILayout.Button("Export to STL"))
                ExportSTL();
        }

        private void ExportSTL()
        {
            var meshFilters = rotators.SelectMany(r => r.GetComponentsInChildren<MeshFilter>())
                                      .Union(GroundObject.GetComponentsInChildren<MeshFilter>())
                                      .ToArray();

            //Back to STL file
            var path = Application.dataPath + "/StreamingAssets/STLFiles/output.stl";

            if (File.Exists(path))
                File.Delete(path);

            var fileStream = new FileStream(path, FileMode.OpenOrCreate);

            MeshToSTL.ConvertMeshToSTL(fileStream, true, meshFilters);

            fileStream.Close();
            fileStream.Dispose();

            Debug.Log("STL written to: " + path);
        }

        private class Rotator : MonoBehaviour
        {
            private void Update()
            {
                transform.rotation *= Quaternion.Euler(new Vector3(1, 1) * Time.deltaTime * 10);
            }

            public void Drop()
            {
                gameObject.AddComponent<Rigidbody>();
                gameObject.AddComponent<BoxCollider>();

                //Stop rotation
                this.enabled = false;
            }
        }
    }
}