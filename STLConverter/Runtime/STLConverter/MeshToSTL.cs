using System;
using System.IO;
using dk.happyhobbies.stlconverter.Internal;
using UnityEngine;

namespace dk.happyhobbies.stlconverter
{
    public static class MeshToSTL
    {
        public static void ConvertMeshToSTL(Stream outputStream, bool binary, params MeshFilter[] input)
        {
            STLWriter stlWriter = null;
            try
            {
                stlWriter = binary ? new STLBinaryWriter(outputStream, input[0].name) : new STLAsciiWriter(outputStream, input[0].name) as STLWriter;

                stlWriter.WriteHeader();


                foreach (var meshFilter in input)
                {
                    var triangles = meshFilter.mesh.triangles;
                    var vertices = meshFilter.mesh.vertices;

                    //Convert from local space to world space (Translation, Rotation and Scaling) 
					var localToWorldMatrix = meshFilter.transform.localToWorldMatrix;
                    for (int i = 0; i < vertices.Length; i++)
                        vertices[i] = localToWorldMatrix.MultiplyPoint3x4(vertices[i]);

                    //Debug.Log("Number of vertices: " + vertices.Length);
                    //Debug.Log("Number of triangles: " + triangles.Length);
                    //for (int i = 0; i < triangles.Length; i += 3)
                    for (int i = triangles.Length - 3; i >= 0; i -= 3)
                    {
                        stlWriter.WriteTriangle(vertices[triangles[i]], vertices[triangles[i + 1]], vertices[triangles[i + 2]]);
                    }
                }

                stlWriter.WriteFooter();
            }
            finally
            {
                stlWriter.Dispose();
            }
        }
    }
}
