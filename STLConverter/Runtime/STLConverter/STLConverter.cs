using System;
using System.Collections.Generic;
using System.IO;
using dk.happyhobbies.stlconverter.Internal;
using UnityEngine;

namespace dk.happyhobbies.stlconverter
{
    public static class STLConverter
	{
		public static Mesh ConvertSTLToMesh(byte[] rawSTLFile)
		{
			//Invert x and the indices
			ISTLParser parser = new STLASCIIParser();
			var model = parser.Deserialize(rawSTLFile);

            var mesh = new Mesh { name = model.Name };

            var indices = new List<int>();
			var vertices = new List<Vector3>();
			var triangles = new List<int>();
			var vertexRepo = new List<Vector3>();

			for (int i = 0; i < model.Triangles.Count; i++)
			{
				//var temp = new int[] { indices.Count, indices.Count + 1, indices.Count + 2 };
				var rawVertices = model.Triangles[i].Vertices;
				var triIncides = new int[] {    AddVertex(vertexRepo, rawVertices[2]),
												AddVertex(vertexRepo, rawVertices[1]),
												AddVertex(vertexRepo, rawVertices[0])};
				triangles.AddRange(triIncides);
			}

			vertices.AddRange(vertexRepo);

			mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
			mesh.SetVertices(vertices);
			mesh.SetTriangles(triangles.ToArray(), 0);

			mesh.RecalculateBounds();
			mesh.RecalculateNormals();

			return mesh;
		}

		private static int AddVertex(List<Vector3> vertexRepo, Vector3 newVertex)
		{
			var newVert = new Vector3(-newVertex.x, newVertex.y, newVertex.z);
			vertexRepo.Add(newVert);

			return vertexRepo.Count - 1;
		}

		public static STLModel ConvertTextureToSTLModel(Texture2D inputTexture)
		{
			var model = new STLModel();

			var colors = inputTexture.GetPixels(miplevel: 0);

			var grayScales = Array.ConvertAll(colors, c => c.grayscale * 20);
			Func<int, int, float> getGrayScale = (x, y) => grayScales[x + y * inputTexture.width];
				
			Action<Vector3, Vector3, Vector3> addTrianlge = (vert0, vert1, vert2) =>
			 {
				 var triangle = new Triangle();
				 triangle.Vertices = new Vector3[] { vert0, vert1, vert2 };
				 model.Triangles.Add(triangle);
			 };

			Action<Vector3, Vector3, Vector3, Vector3> addQuad = (vert0, vert1, vert2, vert3) =>
			{
				addTrianlge(vert0, vert1, vert2);
				addTrianlge(vert0, vert2, vert3);
			};

			for (var x = 0; x < inputTexture.width - 1; x ++)
			{
				for (var y = 0; y < inputTexture.height - 1; y++)
				{
					//Make vertex repo?
					var vert0 = new Vector3(x, y, 			getGrayScale(x, y));
					var vert1 = new Vector3(x, y + 1, 		getGrayScale(x, y + 1));
					var vert2 = new Vector3(x + 1, y + 1, 	getGrayScale(x + 1, y +1));
					var vert3 = new Vector3(x + 1, y, 		getGrayScale(x + 1, y));

					addQuad(vert0, vert1, vert2, vert3);
				}
			}

			return model;
		}
	}
}