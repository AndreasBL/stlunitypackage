using System;
using System.IO;
using UnityEngine;


namespace dk.happyhobbies.stlconverter.Internal
{
    public abstract class STLWriter : IDisposable
    {
 
        protected string modelName;

        protected Stream stream;

        protected STLWriter(Stream stream, string modelName)
        {
            this.modelName = modelName;
            this.stream = stream;
        }

        public abstract void WriteHeader();
        public abstract void WriteTriangle(params Vector3[] vertices);
        public abstract void WriteFooter();

        /// <summary>
        /// Transforms the a vector from unity to STL space.
        /// </summary>
        /// <param name="verts">Verts.</param>
        protected void TransformVectorToSTLSpace(ref Vector3[] verts)
        {
            //Invert sign on x
            for (int i = 0; i < 3; i++)
                verts[i].x = -verts[i].x;

            //Swap first and last vert
            var newFirst = verts[2];
            verts[2] = verts[0];
            verts[0] = newFirst;
        }

		protected Vector3 CalculateNormal(params Vector3[] vertices)
		{
			var side1 = vertices[1] - vertices[2];
			var side2 = vertices[0] - vertices[2];

			var normal = Vector3.Cross(side1, side2);
			normal.Normalize();

            return normal;
		}

        public virtual void Dispose()
        {
			//stream.Flush();
   //         stream.Close();
			//stream.Dispose();
        }
    }
}
