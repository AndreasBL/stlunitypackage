using System;
using System.IO;
using UnityEngine;

namespace dk.happyhobbies.stlconverter.Internal
{
    /// <summary>
    /// Spec are found at https://en.wikipedia.org/wiki/STL_(file_format)
    /// But in short:
    /// 
    /// UINT8[80] – Header
    /// UINT32 – Number of triangles
    ///
    /// foreach triangle
    /// REAL32[3] – Normal vector
    /// REAL32[3] – Vertex 1
    /// REAL32[3] – Vertex 2
    /// REAL32[3] – Vertex 3
    /// UINT16 – Attribute byte count
    /// end
    /// 
    /// </summary>
    public class STLBinaryWriter : STLWriter
    {
        private UInt32 triangleCount;
        private readonly BinaryWriter binaryWriter;

        public STLBinaryWriter(Stream stream, string modelName) : base(stream, modelName)
        {
            binaryWriter = new BinaryWriter(stream);
        }

        public override void WriteHeader()
        {
            const int headerSize = 80;
            const int numberOfTrisFloat = 4;
            var header = new char[headerSize + numberOfTrisFloat];
            binaryWriter.Write(header);
        }

        public override void WriteTriangle(params Vector3[] vertices)
        {
            Action<Vector3> writeVector = vectorToWrite =>
            {
                binaryWriter.Write(vectorToWrite.x);
                binaryWriter.Write(vectorToWrite.y);
                binaryWriter.Write(vectorToWrite.z);
            };

            var normal = CalculateNormal(vertices);
            writeVector(normal);

            TransformVectorToSTLSpace(ref vertices);

            for (int i = 0; i < 3; i++)
                writeVector(vertices[i]);

            binaryWriter.Write(new char[2]);

            triangleCount++;
        }

        public override void WriteFooter()
        {
            binaryWriter.BaseStream.Seek(80, SeekOrigin.Begin);
            binaryWriter.Write(triangleCount);
            Debug.Log("total tris: " + triangleCount);
        }

        public override void Dispose()
        {
            binaryWriter.Flush();
            binaryWriter.Close();
            base.Dispose();
        }

    }
}
