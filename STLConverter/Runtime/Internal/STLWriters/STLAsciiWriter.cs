using System;
using System.IO;
using UnityEngine;

namespace dk.happyhobbies.stlconverter.Internal
{
    public class STLAsciiWriter : STLWriter
    {
        private StreamWriter streamWriter;

        public STLAsciiWriter(Stream stream, string modelName) : base(stream, modelName)
        {
			streamWriter = new StreamWriter(stream);
        }

        public override void WriteHeader()
        {
            streamWriter.WriteLine("solid " + modelName ?? "Exported");
        }

        public override void WriteTriangle(params Vector3[] vertices)
        {
            var normal = CalculateNormal(vertices);

			streamWriter.WriteLine(" facet normal " + normal.x + " " + normal.y + " " + normal.z);
			streamWriter.WriteLine("  outer loop");

            TransformVectorToSTLSpace(ref vertices);

            for (int i = 0; i < 3; i++)
                streamWriter.WriteLine("  vertex " + vertices[i].x + " " + vertices[i].y + " " + vertices[i].z);
            
			streamWriter.WriteLine("  endloop");
			streamWriter.WriteLine(" endfacet");
        }


        public override void WriteFooter()
		{
            streamWriter.WriteLine("endsolid " + modelName ?? "Exported");
		}

        public override void Dispose()
        {
            streamWriter.Flush();
            streamWriter.Close();
            streamWriter.Dispose();
            base.Dispose();
        }
    }
}
