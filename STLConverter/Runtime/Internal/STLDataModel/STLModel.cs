using System.Collections.Generic;
using UnityEngine;

namespace dk.happyhobbies.stlconverter.Internal
{
	public class STLModel
	{
		public string Name;
		public List<Triangle> Triangles = new List<Triangle>();
	}

	public class Triangle
	{
		public Vector3 Normal;
		public Vector3[] Vertices = new Vector3[3];
	}
}