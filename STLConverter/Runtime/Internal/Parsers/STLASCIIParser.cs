using System;
using System.Text;
using UnityEngine;

namespace dk.happyhobbies.stlconverter.Internal
{
	public class STLASCIIParser : ISTLParser
	{
		public STLModel Deserialize(byte[] rawSTLFile)
		{
			var model = new STLModel();

			var stlString = Encoding.UTF8.GetString(rawSTLFile);
			var stlLines = stlString.Split('\n');
			model.Name = stlLines[0].Replace("solid", "").Trim();

			for (int index = 1; index < stlLines.Length - 1; index++)
			{
				var trimmed = stlLines[index].Trim();

				if (trimmed.Length == 0)
					continue;

				if (trimmed[0] == 'f')
				{
					model.Triangles.Add(ParseTriangle(stlLines, index));
					index += 6;
				}
			}

			UnityEngine.Debug.Log(model.Name + " " + model.Triangles.Count);
			return model;
		}


		private Triangle ParseTriangle(string[] stlLines, int startIndex)
		{
			var triangle = new Triangle();
			triangle.Normal = ParseStringToVector(stlLines[startIndex].Replace("facet normal", "").Trim());

			triangle.Vertices[0] = ParseStringToVector(stlLines[startIndex + 2].Replace("vertex", "").Trim());
			triangle.Vertices[1] = ParseStringToVector(stlLines[startIndex + 3].Replace("vertex", "").Trim());
			triangle.Vertices[2] = ParseStringToVector(stlLines[startIndex + 4].Replace("vertex", "").Trim());

			return triangle;
		}

		private Vector3 ParseStringToVector(string inputString)
		{
			var vector = new Vector3();
			var tokens = inputString.Split(' ');

			vector.x = float.Parse(tokens[0]);
			vector.y = float.Parse(tokens[1]);
			vector.z = float.Parse(tokens[2]);

			return vector;
		}

		public string Serialize(STLModel model)
		{
			var stringBuilder = new StringBuilder();

			stringBuilder.AppendLine("solid " + model.Name ?? "Exported");

			var triangles = model.Triangles;

			Debug.Log("Number of triangles: " + triangles.Count);
			for (int i = 0; i < triangles.Count; i++)
			{
				var verts = triangles[i].Vertices;
				AddSTLTrianlge(stringBuilder, verts[0], verts[1], verts[2]);
			}

			stringBuilder.AppendLine("endsolid " + model.Name ?? "Exported"); ;
			return stringBuilder.ToString();
		}

		private static void AddSTLTrianlge(StringBuilder stringBuilder, params Vector3[] vertices)
		{
			var side1 = vertices[1] - vertices[2];
			var side2 = vertices[0] - vertices[2];

			var normal = Vector3.Cross(side2, side1); //reverse 
			normal.Normalize();

			stringBuilder.AppendLine(" facet normal " + normal.x + " " + normal.y + " " + normal.z);
			stringBuilder.AppendLine("  outer loop");

			stringBuilder.AppendLine("  vertex " + vertices[0].x + " " + vertices[0].y + " " + vertices[0].z);
			stringBuilder.AppendLine("  vertex " + vertices[1].x + " " + vertices[1].y + " " + vertices[1].z);
			stringBuilder.AppendLine("  vertex " + vertices[2].x + " " + vertices[2].y + " " + vertices[2].z);


			stringBuilder.AppendLine("  endloop");
			stringBuilder.AppendLine(" endfacet");
			stringBuilder.AppendLine();
		}
	}
}
