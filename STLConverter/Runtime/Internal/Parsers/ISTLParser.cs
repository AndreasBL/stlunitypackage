using System;

namespace dk.happyhobbies.stlconverter.Internal
{
	public interface ISTLParser
	{
		STLModel Deserialize(byte[] rawSTLFile);
		string Serialize(STLModel model);
	}
}
